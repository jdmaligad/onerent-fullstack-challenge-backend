# The Property Directory

A full-stack coding challenge submission to Onerent by Juan Alphonso D. Maligad

## Mutation Queries

    mutation {
        createUser(firstName: "Firstname", lastName: "Lastname") {
            id
            firstName
            lastName
    }
    }

    mutation {
        createProperty(street: "199 Object Street", city: "Something City", state: "San Francisco State", zip: "SF1003", rent: 3800) {
            id
            street,
            city,
            state,
            zip,
            rent
    }
    }