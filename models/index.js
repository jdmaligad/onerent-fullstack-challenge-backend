const Sequelize = require('sequelize');

const sequelize = new Sequelize('onerent-01', 'postgres', 'root', {
    dialect: 'postgres',
    defiine: { 
        underscored: true
    }
});

const models = {
    User: require('./user')(sequelize,Sequelize),
    Property: require('./property')(sequelize,Sequelize)
}

Object.keys(models).forEach((modelName) => {
    if ('associate' in models[modelName]) {
        models[modelName].associate(models);
    }
});

models.sequelize = sequelize
models.Sequelize = Sequelize;

module.exports = models;