module.exports = (sequelize, DataTypes) => {
  const Property = sequelize.define('property', {
    id: {
        type: DataTypes.STRING,
        unique: true,
        primaryKey: true,
        allowNull: false
    },
    street: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    zip: DataTypes.STRING,
    rent: DataTypes.INTEGER,
  })

  Property.associate = (models) => {
    models.Property.belongsTo(models.User, {
      as: 'User',
      onDelete: 'CASCADE',
      foreignKey: 'userId'
    });
  }

  return Property;
};