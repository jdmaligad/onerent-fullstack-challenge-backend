const { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
    type User {
        id: String!
        firstName: String!
        lastName: String!
    }

    type Property {
        id: String!
        street: String!
        city: String!
        state: String!
        zip: String!
        rent: Int!
    }

    type Query {
        search(args: String): [Property!]!
        getUser(id: Int!): User!
        getProperty(id: Int!): Property!
    }

    type Mutation {
        createUser(firstName: String!, lastName: String!): User!
        createProperty(street: String!, city: String!, state: String!, zip: String!, rent: Int!): Property
    }
`);

// The root provides a resolver function for each API endpoint
var root = {
    // Query
    search: (args, { models }) => models.findAll({ where: {} }),
    getUser: ({ id }, { models }) => models.findOne({ where: {id} }),
    getProperty: ({ id }, { models }) => models.findOne({ where: {id} }),

    // Mutation
    createUser: (args, { models }) => models.User.create(args),
    createProperty: (args, { models }) => models.Property.create(args)
};

// module.exports = new GraphQLSchema({
//     query: RootQuery
// })
// const axios = require('axios');
// const { 
//     GraphQLObjectType, 
//     GraphQLString, 
//     GraphQLInt,
//     GraphQLList,
//     GraphQLSchema
// } = require('graphql');
// const models = require('./models')

// // User
// // - id (string) - "5592d311d7c6770300911b65"
// // - firstName (String) - "John"
// // - lastName (String) - "Smith"

// // Property
// // - id (string) - "5592d311d7c6770300911b65"
// // - street (string) - "505 South Market St"
// // - city (string) - "San Jose"
// // - state (string) - "CA"
// // - zip (string) - "95008"
// // - rent (number) - 3500

// const testData = [
//     {
//         id: 0,
//         firstName: "Jam",
//         lastName: "Maligad",
//         property: null
//     },
//     {
//         id: 1,
//         firstName: "Adrheine",
//         lastName: "Maligad",
//         property: null
//     }
// ]

// // User Type
// const UserType = new GraphQLObjectType({
//     name: 'User',
//     fields: () => ({
//         id: { type: GraphQLString },
//         firstName: { type: GraphQLString },
//         lastName: { type: GraphQLString },
//     })
// })

// //  Property Type
// const PropertyType = new GraphQLObjectType({
//     name: 'Property',
//     fields: () => ({
//         id: { type: GraphQLString },
//         user: { type: UserType },
//         street: { type: GraphQLString },
//         city: { type: GraphQLString },
//         state: { type: GraphQLString },
//         zip: { type: GraphQLString },
//         rent: { type: GraphQLInt }
//     })
// })

// // Search Type
// // const SearchType = new GraphQLObjectType({
// //     name: 'Search',
// //     fields 
// // })

// // Root Query
// const RootQuery = new GraphQLObjectType({
//     name: 'SearchQueryType',
//     fields: {
//         users: {
//             type: new GraphQLList(UserType),
//             resolve(parent, args) {
//                 // return axios request
//                 return test_users;
//             }
//         },
//         search: {
//             type: new GraphQLList(PropertyType),
//             args: {
//                 value: { type: GraphQLString }
//             },
//             resolve(parent, args) {
//                 return test_users.filter((test_users) => test_users.lastName  args.value);
//             }
//         }
//     }
// })

// module.exports = new GraphQLSchema({
//     query: RootQuery
// })