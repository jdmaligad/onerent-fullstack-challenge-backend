const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const models = require('./models');
const { buildSchema } = require('graphql');
const { Sequelize } = require('sequelize');
const { v4 } = require('uuid');
const cors = require('cors');
const {or, in: opIn, iLike, substring} = Sequelize.Op;


var schema = buildSchema(`
    type User {
        id: String!
        firstName: String!
        lastName: String!
        properties: [Property]
    }

    type Property {
        id: String!
        street: String!
        city: String!
        state: String!
        zip: String!
        rent: Int!
        User: User
    }

    type Query {
        search(value: String): [Property!]!
        getUser(id: String!): User!
        getProperty(id: String!): Property!
    }

    type Mutation {
        createUser(firstName: String!, lastName: String!): User!
        createProperty(street: String!, city: String!, state: String!, zip: String!, rent: Int!, userId: String): Property
    }
`);

// The root provides a resolver function for each API endpoint
var root = {
    // Query
    search: async (args, { models }) => {
      console.log(args.value);
      try {
        const results = await models.Property.findAll({
          where: {
            [or]: [
              { 
                street: {
                  [iLike]: '%'+args.value+'%'
                }
              },
              {
                street: {
                  [substring]: args.value
                }
              },
              {
                street: {
                  [opIn]: args.value.split(" ")
                }
              },
              {
                street: {
                  [opIn]: args.value.split(",")
                }
              },
              { 
                city: {
                  [iLike]: '%'+args.value+'%'
                }
              },
              {
                city: {
                  [substring]: args.value
                }
              },
              {
                city: {
                  [opIn]: args.value.split(" ")
                }
              },
              {
                city: {
                  [opIn]: args.value.split(",")
                }
              },
              { 
                state: {
                  [iLike]: '%'+args.value+'%'
                }
              },
              {
                state: {
                  [substring]: args.value
                }
              },
              {
                state: {
                  [opIn]: args.value.split(" ")
                }
              },
              {
                state: {
                  [opIn]: args.value.split(",")
                }
              },
              { 
                zip: {
                  [iLike]: '%'+args.value+'%'
                }
              },
              {
                zip: {
                  [substring]: args.value
                }
              },
              {
                zip: {
                  [opIn]: args.value.split(" ")
                }
              },
              {
                '$User.firstName$': {
                  [iLike]: '%'+args.value+'%'
                }
              },
              {
                '$User.firstName$': {
                  [substring]: args.value
                }
              },
              {
                '$User.firstName$': {
                  [opIn]: args.value.split(" ")
                }
              },
              {
                '$User.lastName$': {
                  [iLike]: '%'+args.value+'%'
                }
              },
              {
                '$User.lastName$': {
                  [substring]: args.value
                }
              },
              {
                '$User.lastName$': {
                  [opIn]: args.value.split(" ")
                }
              },
            ],
          },
          include: [{ model: models.User, as: 'User' }]
        })

        return results;
      } catch (err) {
        console.log(err);
        return null;
      }
    },
    getUser: ({ id }, { models }) => models.User.findOne({ where: {id}, include: [ { model: models.Property } ] }),
    getProperty: ({ id }, { models }) => models.Property.findOne({ where: {id}, include: { model: models.User } }),

    // Mutation
    createUser: (args, { models }) => {
      args.id = v4().toString();
      return models.User.create(args);
    },
    createProperty: (args, { models }) => {
      args.id = v4().toString();
      models.Property.addUser
      return models.Property.create(args)
    }
};


const app = express();
app.use(cors('*'));
app.use(
    '/graphql',
    graphqlHTTP({
      schema: schema,
      context: {
        models
      },
      rootValue: root,
      graphiql: true
    }),
  );
  
const PORT = process.env.PORT || 5000;

models.sequelize.sync({}).then(() => {
  app.listen(PORT, () => console.log(`Server is listening on port ${PORT}`));
})